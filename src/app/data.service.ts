import { ProductModel } from './models/product.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public url = 'http://www.mocky.io/v2/5d97a4823b00006600c31795';

  constructor(private http: HttpClient) {}

  getProducts() {
    return this.http.get<ProductModel[]>(`${this.url}`);
  }
}
